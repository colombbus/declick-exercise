import '../../node_modules/codemirror/lib/codemirror.css'
import '../../node_modules/codemirror/theme/abcdef.css'
import '../../node_modules/codemirror/addon/dialog/dialog.css'
import '../style/editor.scss'
import '../../node_modules/codemirror/mode/javascript/javascript.js'
import '../../node_modules/codemirror/mode/markdown/markdown.js'
import '../../node_modules/codemirror/addon/dialog/dialog.js'
import CodeMirror from 'codemirror'

export class Editor {
  constructor(element) {
    this._element = element
    this._documents = new Map()
    this._editor = CodeMirror(element, {
      lineNumbers: true,
      theme: 'abcdef',
    })
  }

  reduce() {
    if (this._element.classList.contains('full')) {
      this._element.classList.remove('full')
    } else if (!this._element.classList.contains('close')) {
      this._element.classList.add('close')
    }
  }

  extend() {
    if (this._element.classList.contains('close')) {
      this._element.classList.remove('close')
    } else if (!this._element.classList.contains('full')) {
      this._element.classList.add('full')
    }
  }

  isClosed() {
    return this._element.classList.contains('close')
  }

  getValue() {
    return this._editor.getValue()
  }

  setValue(value) {
    return this._editor.setValue(value)
  }

  notify(text, type) {
    const div = document.createElement('div')
    div.innerHTML = text
    div.className = type
    this._editor.openNotification(div, { bottom: true })
  }

  createDocument(name, type) {
    this._documents.set(name, CodeMirror.Doc('', type))
  }

  setDocumentValue(name, value) {
    if (!this._documents.has(name)) {
      this._documents.set(name, CodeMirror.Doc(''))
    }
    this._documents.get(name).setValue(value)
  }

  getDocumentValue(name) {
    if (this._documents.has(name)) {
      return this._documents.get(name).getValue()
    }
    return ''
  }

  displayDocument(name) {
    if (!this._documents.has(name)) {
      this._documents.set(name, CodeMirror.Doc(''))
    }
    this._editor.swapDoc(this._documents.get(name))
  }

  onBlur(handler) {
    this._editor.on('blur', handler)
  }
}
