import JSZip from 'jszip'
import fxparser from 'fast-xml-parser'
import { getBase64, getText, getFileName } from './localFS'

let jsZip = null

async function loadTemplate() {
  const response = await fetch('scorm.zip', { method: 'GET' })
  const data = await response.arrayBuffer()
  jsZip = new JSZip()
  return await jsZip.loadAsync(data)
}

function addResources(data, zip) {
  const resources = {}
  let fileName, fileName2
  const assets = zip.folder('assets')
  for (const [name, resource] of Object.entries(data)) {
    switch (resource[0]) {
      case 'image':
        fileName = getFileName(name)
        assets.file(fileName, getBase64(fileName), { base64: true })
        resources[name] = ['image', 'assets/' + fileName]
        break
      case 'spritesheet':
        fileName = getFileName(name)[0]
        fileName2 = getFileName(name)[1]
        assets.file(fileName, getBase64(fileName), { base64: true })
        assets.file(fileName2, getText(fileName2))
        resources[name] = [
          'spritesheet',
          'assets/' + fileName,
          'assets/' + fileName2,
        ]
        break
      case 'map':
        fileName = getFileName(name)
        assets.file(fileName, getText(fileName))
        resources[name] = ['map', 'assets/' + fileName]
        break
    }
  }
  return resources
}

async function getSCORMFile(exercise) {
  const zip = await loadTemplate()
  const manifestFile = zip.file('imsmanifest.xml')
  if (!manifestFile) {
    throw 'no manifest found in template'
  }
  const manifestData = await manifestFile.async('string')
  const manifest = fxparser.parse(manifestData, { ignoreAttributes: false })
  manifest['@_identifier'] = 'colombbus.declick.exercise.' + exercise.getName()
  manifest['@_version'] = exercise.getVersion()
  manifest.manifest.organizations.organization.title =
    'Declick - ' + exercise.getName()
  manifest.manifest.organizations.organization.item.title = exercise.getTitle()
  const exerciseData = {
    name: exercise.getName(),
    instructions: exercise.getInstructions(),
    init: exercise.getInitScript(),
    code: exercise.getStartCode(),
    pre: exercise.getPreScript(),
    post: exercise.getPostScript(),
    check: exercise.getCheckScript(),
    title: exercise.getTitle(),
    resources: addResources(exercise.getResources(), zip),
  }
  const files = []
  zip.forEach((path, file) => {
    if (!file.dir) {
      files.push({
        '@_href': path,
      })
    }
  })
  manifest.manifest.resources.resource.file = files
  const xmlParser = new fxparser.j2xParser({ ignoreAttributes: false })
  const newManifest = '<?xml version="1.0" ?>\n' + xmlParser.parse(manifest)
  zip.file('imsmanifest.xml', newManifest)
  zip.file('exercise.json', JSON.stringify(exerciseData))
  return await zip.generateAsync({ type: 'blob' })
}

async function loadSCORMFile(fileContent) {
  jsZip = new JSZip()
  const zipFile = await jsZip.loadAsync(fileContent)
  const exerciseFile = zipFile.file('exercise.json')
  if (!exerciseFile) {
    throw 'exercise.json not found'
  }
  const JSONdata = await exerciseFile.async('string')
  const data = JSON.parse(JSONdata)
  data.zipFile = zipFile
  return data
}

export { getSCORMFile, loadSCORMFile }
