const checkObject = function (model, object) {
  const ignoreKeys = ['start', 'raw', 'end', 'loc']
  const keys = Object.keys(model).filter(key => !ignoreKeys.includes(key))
  for (const key of keys) {
    if (object[key] === undefined) {
      return false
    }
    if (Array.isArray(model[key])) {
      if (!checkBlock(model[key], object[key])) {
        return false
      }
    } else if (typeof model[key] === 'object' && model[key] !== null) {
      if (!checkObject(model[key], object[key])) {
        return false
      }
    } else {
      if (model[key] !== object[key]) {
        return false
      }
    }
  }
  return true
}
const checkBlock = function (modelBlock, block) {
  if (!Array.isArray(block)) {
    return false
  }
  if (modelBlock.length !== block.length) {
    return false
  }
  for (let i = 0; i < modelBlock.length; i++) {
    const element = modelBlock[i]
    if (Array.isArray(element)) {
      if (!checkBlock(element, block[i])) {
        return false
      }
    } else if (typeof element === 'object') {
      if (!checkObject(element, block[i])) {
        return false
      }
    } else {
      if (element !== block[i]) {
        return false
      }
    }
  }
  return true
}
class Inspector {
  constructor(declick, value) {
    this.declick = declick
    if (typeof value === 'string' || value instanceof String) {
      this.statements = this.declick.parse(value).body
    } else {
      this.statements = value
    }
  }

  get length() {
    return this.statements.length
  }

  getBody(index) {
    if (index > this.length) {
      return null
    }
    const statement = this.statements[index]
    let body = null
    switch (statement.type) {
      case 'RepeatStatement':
        body = statement.body.body
        break
      case 'IfStatement':
        body = statement.consequent.body
        break
    }
    if (body) {
      return new Inspector(this.declick, body)
    }
    return null
  }

  getStatement(index) {
    if (index > this.length) {
      return null
    }
    return this.statements[index]
  }

  getStatementType(index) {
    if (index > this.length) {
      return null
    }
    return this.statements[index].type
  }

  isLike(code) {
    const parsed = this.declick.parse(code)
    if (parsed && parsed.body) {
      return checkBlock(parsed.body, this.statements)
    }
    return false
  }

  has(statementToFind) {
    return (
      this.statements.find(statement =>
        checkObject(statementToFind, statement),
      ) !== undefined
    )
  }

  debug(index) {
    if (index !== undefined) {
      console.log(this.statements[index])
    } else {
      console.log(this.statements)
    }
  }
}

class Assessor {
  constructor(declick) {
    this.declick = declick
  }

  assess(code, check) {
    const input = []
    let inputNames = []
    if (check.length === 0) {
      return 'no check script'
    }
    const start = check.substring(0, check.indexOf('\n'))
    if (start.length > 0 && start.replace(/\s+/g, '').startsWith('//require')) {
      inputNames = start.replace(/\s+/g, '').substring(9).split(',')
      inputNames.forEach(name => {
        try {
          let value = this.declick.getDeclickObject(name)
          if (value === null) {
            value = this.declick.getDeclickVariable(name)
          }
          input.push(value)
        } catch (e) {
          return `cannot find Declick object or variable ${name} - ${e}`
        }
      })
    }
    let test
    try {
      test = new Function('code', ...inputNames, check)
    } catch (e) {
      return `function error - ${e}`
    }
    try {
      return test(new Inspector(this.declick, code), ...input)
    } catch (e) {
      return `execution error - ${e}`
    }
  }
}

export { Assessor }
