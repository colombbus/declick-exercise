export class Resource {
  constructor(name = null, data = null) {
    if (name && name instanceof Resource) {
      this.name = name.name
      this.type = name.type
      this.file1 = name.file1
      this.file2 = name.file2
    } else {
      this.name = name ? name : ''
      this.type = data ? data[0] : 'image'
      this.file1 = data ? data[1] : false
      if (this.type && this.type === 'spritesheet') {
        this.file2 = data ? data[2] : false
      } else {
        this.file2 = false
      }
    }
  }

  getName() {
    return this.name
  }

  getDisplayName() {
    return `${this.name} (${this.type})`
  }

  getLocalData(localFS) {
    switch (this.type) {
      case 'image':
        return ['image', localFS.getURL(this.file1)]
      case 'spritesheet':
        return [
          'spritesheet',
          [localFS.getURL(this.file1), localFS.getJSON(this.file2)],
        ]
      case 'map':
        return ['map', localFS.getJSON(this.file1)]
    }
    return []
  }
}
