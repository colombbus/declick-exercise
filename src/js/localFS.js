const reader = new FileReader()
const request = new XMLHttpRequest()

const filesToLoad = []
const fileNames = new Map()

let currentFileName
let loading = false
let loadedCallback = null

reader.addEventListener(
  'load',
  function () {
    if (reader.result) {
      localStorage.setItem(currentFileName, reader.result.toString())
    }
    loadNextFile()
  },
  false,
)

function deleteFile(fileName) {
  localStorage.removeItem(fileName)
}

function loadNextFile() {
  if (filesToLoad.length === 0) {
    stopLoading()
  } else {
    const { fileName, path, method, file } = filesToLoad.shift()
    currentFileName = fileName
    let dataType = ''
    let image = false
    if (fileName.endsWith('.jpeg') || fileName.endsWith('.jpg')) {
      image = true
      dataType = 'image/jpeg'
    } else if (fileName.endsWith('.png')) {
      image = true
      dataType = 'image/png'
    } else if (fileName.endsWith('.gif')) {
      image = true
      dataType = 'image/gif'
    }
    switch (method) {
      case 'get': {
        request.open('GET', path, true)
        request.responseType = 'blob'
        request.onload = function () {
          if (image) {
            reader.readAsDataURL(request.response)
          } else {
            reader.readAsText(request.response)
          }
        }
        request.send()
        break
      }
      case 'zip': {
        const assetFile = file.file(path)
        assetFile.async('blob').then(data => {
          if (image) {
            const test = new Blob([data], { type: dataType })
            reader.readAsDataURL(test)
          } else {
            reader.readAsText(data)
          }
        })
        break
      }
      case 'file': {
        if (image) {
          reader.readAsDataURL(file)
        } else {
          reader.readAsText(file)
        }
        break
      }
    }
  }
}

function stopLoading() {
  if (loading) {
    loading = false
    if (loadedCallback !== null) {
      const callback = loadedCallback
      loadedCallback = null
      callback()
    }
  }
}

function startLoading() {
  if (!loading) {
    loading = true
    loadNextFile()
  }
}

function addFileName(name, fileName) {
  if (fileNames.has(name)) {
    let current = fileNames.get(name)
    if (!Array.isArray(current)) {
      current = [current]
    }
    current.push(fileName)
    fileNames.set(name, current)
  } else {
    fileNames.set(name, fileName)
  }
}

function loadFile(
  name,
  fileName,
  path,
  startNow = true,
  method = 'get',
  file = null,
) {
  addFileName(name, fileName)
  filesToLoad.push({
    fileName,
    path,
    method,
    file,
  })
  if (startNow) {
    if (!loading) {
      startLoading()
    }
  }
}

function getFileName(name) {
  return fileNames.get(name)
}

function updateFileName(oldName, newName) {
  fileNames.set(newName, fileNames.get(oldName))
  fileNames.delete(oldName)
}

function getURL(name) {
  return localStorage.getItem(name)
}

function getBase64(name) {
  const value = localStorage.getItem(name)
  if (value) {
    return value.replace(/^data:image\/(png|jpg);base64,/, '')
  }
}

function getJSON(name) {
  const value = localStorage.getItem(name)
  if (value) {
    return JSON.parse(value)
  }
}

function getText(name) {
  return localStorage.getItem(name)
}

function whenLoaded(callback) {
  loadedCallback = callback
}

function clear() {
  localStorage.clear()
  fileNames.clear()
}

function setFileName(name, fileName) {
  fileNames.set(name, fileName)
}

export {
  loadFile,
  startLoading,
  whenLoaded,
  getURL,
  getJSON,
  getText,
  clear,
  getFileName,
  getBase64,
  updateFileName,
  setFileName,
  deleteFile,
}
