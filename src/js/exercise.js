export class Exercise {
  constructor() {
    this.title = ''
    this.version = 0
    this.instructions = ''
    this.initScript = ''
    this.startCode = ''
    this.preScript = ''
    this.postScript = ''
    this.checkScript = ''
    this.images = new Map()
    this.spriteSheets = new Map()
    this.maps = new Map()
    this.resources = {}
  }

  setItem(code, value) {
    switch (code) {
      case 'instructions':
        this.instructions = value
        break
      case 'init':
        this.initScript = value
        break
      case 'code':
        this.startCode = value
        break
      case 'pre':
        this.preScript = value
        break
      case 'post':
        this.postScript = value
        break
      case 'check':
        this.checkScript = value
        break
      case 'title':
        this.title = value
        break
    }
  }

  getItem(code) {
    switch (code) {
      case 'instructions':
        return this.instructions
      case 'init':
        return this.initScript
      case 'code':
        return this.startCode
      case 'pre':
        return this.preScript
      case 'post':
        return this.postScript
      case 'check':
        return this.checkScript
      case 'title':
        return this.title
    }
  }

  setInstructions(value) {
    this.instructions = value
  }

  setInitScript(value) {
    this.initScript = value
  }

  setStartCode(value) {
    this.startCode = value
  }

  setPreScript(value) {
    this.preScript = value
  }

  setPostScript(value) {
    this.postScript = value
  }

  setCheckScript(value) {
    this.checkScript = value
  }

  setTitle(value) {
    this.title = value
  }

  setVersion(value) {
    this.version = value
  }

  setImages(value) {
    this.images = value
  }

  setSpriteSheets(value) {
    this.spriteSheets = value
  }

  setMaps(value) {
    this.maps = value
  }

  setResources(value) {
    this.resources = { ...value }
    this.images.clear()
    this.spriteSheets.clear()
    this.maps.clear()
    for (const [name, resource] of Object.entries(this.resources)) {
      switch (resource[0]) {
        case 'image':
          this.images.set(name, resource[1])
          break
        case 'spritesheet':
          this.spriteSheets.set(name, [resource[1], resource[2]])
          break
        case 'map':
          this.maps.set(name, resource[1])
          break
      }
    }
  }

  getResources() {
    return this.resources
  }

  getImages() {
    return this.images
  }

  getSpriteSheets() {
    return this.spriteSheets
  }

  getMaps() {
    return this.maps
  }

  getInstructions() {
    return this.instructions
  }

  getInitScript() {
    return this.initScript
  }

  getStartCode() {
    return this.startCode
  }

  getPreScript() {
    return this.preScript
  }

  getPostScript() {
    return this.postScript
  }

  getCheckScript() {
    return this.checkScript
  }

  getTitle() {
    return this.title
  }

  getVersion() {
    return this.version
  }

  addImage(name, value) {
    this.images.set(name, value)
  }

  addSpriteSheet(name, value) {
    this.spriteSheets.set(name, value)
  }

  addMap(name, value) {
    this.maps.set(name, value)
  }

  getName() {
    return 'declick'
  }

  load(data, resources = false) {
    this.title = data.title ? data.title : ''
    this.version = data.version ? data.version : '1'
    this.instructions = data.instructions ? data.instructions : ''
    this.initScript = data.init ? data.init : ''
    this.startCode = data.code ? data.code : ''
    this.postScript = data.post ? data.post : ''
    this.preScript = data.pre ? data.pre : ''
    this.checkScript = data.check ? data.check : ''
    this.images.clear()
    this.spriteSheets.clear()
    this.maps.clear()
    if (resources !== false) {
      this.setResources(resources)
    } else if (data.resources) {
      this.setResources(data.resources)
    }
  }
}
