import * as localFS from './localFS.js'
import { Resource } from './resource.js'

const PREFIX = 'assets/'

function readFileAsync(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = () => {
      resolve(reader.result)
    }
    reader.onerror = reject
    reader.readAsArrayBuffer(file)
  })
}

function loadResource(resource, fromFile = null, method = 'get') {
  const files = [resource.file1]
  if (resource.file2) {
    files.push(this.file2)
  }
  let fileIndex = 0
  files.forEach(file => {
    switch (method) {
      case 'get': {
        localFS.loadFile(resource.getName(), file, `${PREFIX}${file}`)
        break
      }
      case 'zip': {
        localFS.loadFile(
          resource.getName(),
          file,
          `${PREFIX}${file}`,
          false,
          'zip',
          fromFile,
        )
        break
      }
      case 'file': {
        const currentFile = fromFile[fileIndex]
        if (currentFile) {
          localFS.loadFile(
            resource.getName(),
            file,
            '',
            false,
            'file',
            currentFile,
          )
        }
        fileIndex++
        break
      }
    }
  })
}

function loadResources(resourcesObject, zipFile = null) {
  localFS.clear()

  const resources = []

  for (const [name, data] of Object.entries(resourcesObject)) {
    const resource = new Resource(name, data, zipFile)
    if (zipFile) {
      resource.file1 = resource.file1.slice(PREFIX.length)
      if (resource.file2) {
        resource.file2 = resource.file2.slice(PREFIX.length)
      }
      loadResource(resource, zipFile, 'zip')
    } else {
      loadResource(resource)
    }
    resources.push(resource)
  }
  return new Promise(resolve => {
    localFS.whenLoaded(() => {
      const resourcesData = {}
      resources.forEach(resource => {
        resourcesData[resource.getName()] = resource.getLocalData(localFS)
      })
      resolve(resourcesData)
    })
    localFS.startLoading()
  })
}

function loadResourceFromFiles(resource, files) {
  const previousFileNames = localFS.getFileName(resource.name)
  if (Array.isArray(previousFileNames)) {
    if (files[0]) {
      localFS.deleteFile(previousFileNames[0])
    }
    if (files[1]) {
      localFS.deleteFile(previousFileNames[1])
    }
  } else if (files[0]) {
    localFS.deleteFile(previousFileNames)
  }
  loadResource(resource, files, 'file')
  return new Promise(resolve => {
    localFS.whenLoaded(() => {
      let fileNames
      if (resource.file2) {
        fileNames = [resource.file1, resource.file2]
      } else {
        fileNames = resource.file1
      }
      localFS.setFileName(resource.getName(), fileNames)

      resolve(resource.getLocalData(localFS))
    })
    localFS.startLoading()
  })
}

export { readFileAsync, loadResources, loadResourceFromFiles }
