import { parse } from '../../node_modules/marked/lib/marked.esm.js'
import { Assessor } from './assessor.js'
// @ts-ignore: declick not generated as a module for now
import Declick from 'declick-engine'
import Split from 'split.js'

let exerciseEditor
let exercise
let localResources = false
let approveHandler, saveHandler, exitAndContinueHandler
const assessor = new Assessor(Declick)

function loadInstructions() {
  const instructionsElement = document.getElementById('instructions')
  if (instructionsElement) {
    instructionsElement.innerHTML = parse(exercise.getInstructions())
  }
}

function loadTitle() {
  const titleElement = document.getElementById('title')
  if (titleElement) {
    titleElement.innerHTML = exercise.getTitle()
  }
}

function loadCode() {
  exerciseEditor.setValue(exercise.getStartCode())
}

function setCode(value) {
  exerciseEditor.setValue(value)
}

function loadExercise(value, local = false) {
  exercise = value
  localResources = local
  loadInstructions()
  loadTitle()
  loadCode()
  initExercise()
}

function initExercise() {
  Declick.reset()
  Declick.addImageResources(exercise.getImages(), localResources, true)
  Declick.addSpriteSheetResources(
    exercise.getSpriteSheets(),
    localResources,
    true,
  )
  Declick.addMapResources(exercise.getMaps(), localResources, true)
  return Declick.startGraphics().then(() => {
    Declick.execute(exercise.getInitScript())
  })
}

function restartExercise() {
  Declick.clear()
  return Declick.execute(exercise.getInitScript())
}

function update(part) {
  switch (part) {
    case 'instructions':
      loadInstructions()
      break
    case 'title':
      loadTitle()
      break
    case 'code':
      loadCode()
      break
    case 'resources':
      initExercise()
      break
    default:
      initExercise()
      break
  }
}

function error(message) {
  exerciseEditor.notify(message, 'error')
}

function debug(value) {
  console.debug(value)
}

function setup(editor, config = {}) {
  exerciseEditor = editor
  if (config.approve) {
    approveHandler = config.approve
  }
  if (config.save) {
    saveHandler = config.save
  }
  if (config.exitAndContinue) {
    exitAndContinueHandler = config.exitAndContinue
  }
  const closeElement = document.getElementById('close')
  const okElement = document.getElementById('ok')
  const successElement = document.getElementById('success')
  const goElement = document.getElementById('go')
  const resetElement = document.getElementById('reset')
  if (closeElement && okElement && successElement) {
    closeElement.addEventListener('click', () => {
      successElement.style.display = 'none'
    })
    okElement.addEventListener('click', () => {
      successElement.style.display = 'none'
      if (exitAndContinueHandler) {
        exitAndContinueHandler()
      }
    })
  }

  Split(['#instructions', '#exercise-body'], {
    direction: 'vertical',
    gutterSize: 8,
    sizes: [25, 75],
    minSize: [50, 100],
  })

  return Declick.initialize(document.getElementById('declick-container')).then(
    () => {
      Declick.addExternalFunction('error', error)
      Declick.addExternalFunction('debug', debug)
      if (goElement && resetElement && successElement) {
        goElement.addEventListener('click', function () {
          const code = exerciseEditor.getValue()
          restartExercise().then(() => {
            Declick.execute(exercise.getPreScript()).then(() => {
              Declick.execute(code).then(() => {
                Declick.execute(exercise.getPostScript()).then(() => {
                  setTimeout(() => {
                    const result = assessor.assess(
                      code,
                      exercise.getCheckScript(),
                    )
                    if (result !== true) {
                      exerciseEditor.notify(result, 'error')
                    } else {
                      successElement.style.display = 'block'
                      if (approveHandler) {
                        approveHandler()
                      }
                      if (saveHandler) {
                        saveHandler(exerciseEditor.getValue())
                      }
                    }
                  }, 1000)
                })
              })
            })
          })
          goElement.blur()
        })
        resetElement.addEventListener('click', function () {
          restartExercise()
        })
      }

      Declick.setErrorHandler(function (declickError) {
        error(declickError.getMessage())
      })
    },
  )
}

export { setup, loadExercise, update, setCode }
