import * as loadManager from '../load-manager.js'
import { useState, useEffect, useRef } from 'react'
import { Resource } from '../resource.js'
import * as localFS from '../localFS.js'
import { saveAs } from 'file-saver'

export function Resources(props) {
  const [selected, setSelected] = useState(null)
  const [current, setCurrent] = useState(new Resource())
  const [edition, setEdition] = useState(false)
  const [pendingFiles, setPendingFiles] = useState([null,null])

  const inputFile1 = useRef(null)
  const inputFile2 = useRef(null)

  useEffect(() => {
    reset()
  },[props.exercise])

  const reset = () => {
    setEdition(false)
    setSelected(null)
    setPendingFiles([null, null])
    setCurrent(new Resource())
  }

  const update = (e) => {
    const newResource = new Resource(current)
    newResource[e.target.name] = e.target.value
    setCurrent(newResource)
  }

  const editResource = (name) => {
    reset()
    setSelected(name)
    const data = [...props.exercise.resources[name]]
    if (data[0] === 'spritesheet') {
      const fileNames = localFS.getFileName(name)
      data[1] = fileNames[0]
      data[2] = fileNames[1]
    } else {
      data[1] = localFS.getFileName(name)
    }
    setCurrent(new Resource(name, data))
    setEdition(true)
  }

  const updateResources = (newResources) => {
    reset()
    props.exercise.setResources(newResources)
    props.onUpdate('resources')
  }

  const deleteResource = (e,name) => {
    if (confirm(`Supprimer la ressource '${name}' ?`)) {
      const currentResources = props.exercise.getResources()
      delete currentResources[selected]
      updateResources(currentResources)
    }
    e.stopPropagation()
  }

  const newResource = () => {
    setSelected(null)
    setCurrent(new Resource())
    setEdition(true)
  }

  const saveResource = async () => {
    const currentResources = props.exercise.getResources()
    if (selected) {
      if (selected !== current.name) {
        if (currentResources[current.name] !== undefined) {
          alert(`Le nom '${current.name}' est déjà utilisé !`)
          return
        }
        delete currentResources[selected]
        localFS.updateFileName(selected, current.name)
      }
    } else {
      if (currentResources[current.name] !== undefined) {
        alert(`Le nom '${current.name}' est déjà utilisé !`)
        return
      }
    }
    if (pendingFiles[0] || pendingFiles[1]) {
      await loadManager.loadResourceFromFiles(current, pendingFiles)
    }
    currentResources[current.name] = current.getLocalData(localFS)
    updateResources(currentResources)
  }

  const downloadResourceFile = (index) => {
    if (pendingFiles[index-1]) {
      alert('Nouveau fichier en attente d\'enregistrement')
    } else {
      const fileName = current[`file${index}`]
      if (fileName) {
        const data = current.getLocalData(localFS)
        let fileData = ''
        if (current.type === 'spritesheet') {
          fileData = data[1][index-1]
        } else {
          fileData = data[1]
        }
        if (typeof fileData === 'object') {
          fileData = new Blob([JSON.stringify(fileData)], {type: "text/plain;charset=utf-8"});
        }
        saveAs(fileData, fileName)
      }
    }
  }

  const uploadResourceFile = (input) => {
    input.current.click()
  }

  const setNewResourceFile = (e, index) => {
    const input = e.target
    const newPendingFiles = [...pendingFiles]
    if (input.files.length > 0) {
      newPendingFiles[index-1] = input.files[0]
      const fileName = input.files[0].name
      current.file1 = fileName
    } else {
      newPendingFiles[index-1] = false
    }
    setPendingFiles(newPendingFiles)
  }

  return (
    <>
      <ul id="build-resources">
        {
          Object.entries(props.exercise.resources).map(([key, value]) =>
            <li key={key} className={selected === key ? 'selected':''} onClick={()=> editResource(key)}>
            {(selected === key) && <i className="icon-trash build-resource-delete" onClick={(e) => deleteResource(e,key)}></i> }
            {`${key} (${value[0]})`}
            </li>
          )
        }
      </ul>
      <form id="build-resource-form" className={edition ? '':'hidden'}>
        <label htmlFor="build-resource-name">Nom</label>
        <input type="text" id="build-resource-name" name="name" value={current.name} onChange={update}/>
        <label htmlFor="build-resource-type">Type</label>
        <select id="build-resource-type" name="type" value={current.type} onChange={update}>
          <option value="image">image</option>
          <option value="spritesheet">spritesheet</option>
          <option value="map">map</option>
        </select>
        <label>Fichier</label>
        <div className={pendingFiles[0]?"build-resource-file pending":"build-resource-file"}>
          <i className="icon-upload build-resource-file-action" onClick={() => uploadResourceFile(inputFile1)}></i>
          <i className="icon-download build-resource-file-action" onClick={()=> downloadResourceFile(1)}></i>
          {current.file1?current.file1:'<aucun>'}
        </div>
        { current.type === 'spritesheet' &&
          <>
            <label>Fichier descriptif</label>
            <div className={pendingFiles[1]?"build-resource-file pending":"build-resource-file"}>
              <i className="icon-upload build-resource-file-action" onClick={() => uploadResourceFile(inputFile2)}></i>
              <i className="icon-download build-resource-file-action" onClick = {() => downloadResourceFile(2)}></i>
              {current.file2?current.file2:'<aucun>'}
            </div>
          </>
        }
        <input type="file" ref={inputFile1} className="hidden" onChange={(e) => setNewResourceFile(e, 1)}/>
        <input type="file" ref={inputFile2} className="hidden" onChange={(e) => setNewResourceFile(e, 2)}/>
      </form>
      <div id="build-resources-actions">
        <button id="build-resource-cancel" className={edition?'':'hidden'} onClick = {reset}>Annuler</button>
        <button id="build-resource-create" className={edition?'hidden':''} onClick = {newResource}>Ajouter...</button>
        <button id="build-resource-save" className={edition?'':'hidden'} onClick = {saveResource}>Enregistrer</button>
      </div>
    </>
  )
}
