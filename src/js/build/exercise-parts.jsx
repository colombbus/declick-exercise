import { Editor } from '../editor.js'
import { Resources } from './resources.jsx'
import { useState, useEffect, useRef } from 'react';

const items = new Map([
  ['title', {type: 'text', label:'Titre'}],
  ['instructions', {type: 'markdown', label:'Consigne'}],
  ['init', {type: 'javascript', label: 'Script d\'initialisation'}],
  ['code', {type: 'javascript', label: 'Code de départ'}],
  ['pre', {type: 'javascript', label: 'Script pré-exécution'}],
  ['post', {type: 'javascript', label: 'Script post-exécution'}],
  ['check', {type: 'javascript', label: 'Script de validation'}]
])

const editableItems = []

for (const [key, value] of items) {
  editableItems.push(<option value={key} key={key}>{value.label}</option>)
}

export function ExerciseParts (props) {

  const editorParent = useRef(null)
  const buildEditor = useRef(null)

  useEffect(() => {
    if (buildEditor.current === null) {
      buildEditor.current = new Editor(editorParent.current)
      for (const [key, value] of items) {
        buildEditor.current.createDocument(key, value.type)
      }
    }

    for (const key of items.keys()) {
      buildEditor.current.setDocumentValue(key, props.exercise.getItem(key))
    }

    buildEditor.current.displayDocument(currentItem)
  })

  const updateExercise = () => {
    props.exercise.setItem(currentItem, buildEditor.current.getDocumentValue(currentItem))
    props.onUpdate(currentItem)
  }

  const [currentItem, setCurrentItem] = useState('title')
  const [activeTab, setActiveTab] = useState(2)

  return (
    <>
      <ul id="build-tabs">
        <li className={activeTab === 1?'active hidden':'hidden'} onClick={()=>setActiveTab(1)}>
          <i className="icon-info-circle" ></i> Infos
        </li>
        <li className={activeTab === 2?'active':''} onClick={()=>setActiveTab(2)}>
          <i className="icon-cogs"></i> Scripts
        </li>
        <li className={activeTab === 3?'active':''} onClick={()=>setActiveTab(3)}>
          <i className="icon-image"></i> Ressources
        </li>
      </ul>
      <div className={activeTab === 1?'build-panes active':'build-panes'} style={ {display:"none"} }>
        bientôt...
      </div>
      <div className={activeTab === 2?'build-panes active':'build-panes'}>
        <select name="build-item" id="build-item" className='build-select' onChange={(e) => setCurrentItem(e.target.value)}>
          {editableItems}
        </select>
        <div className="wrapper">
          <div id="build-editor" className="editor" ref={editorParent} onBlur={updateExercise}></div>
        </div>
      </div>
      <div className={activeTab === 3?'build-panes active':'build-panes'}>
        <Resources exercise={props.exercise} onUpdate={props.onUpdate}/>
      </div>
    </>
  )
}