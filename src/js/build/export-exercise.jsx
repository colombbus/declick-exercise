import * as scormManager from '../scorm-manager.js'
import { saveAs } from 'file-saver'

export function ExportExercise(props) {
  const exportSCORM = async () => {
    const exercise = props.exercise
    const data = await scormManager.getSCORMFile(exercise)
    saveAs(data, exercise.getName() + '.zip')
  }

  return (
    <div id ="scorm-actions">
      <button id="scorm" onClick={exportSCORM}>SCORM</button>
    </div>
  )
}
