import { LoadExercise } from './load-exercise.jsx'
import { ExerciseParts } from './exercise-parts.jsx'
import { ExportExercise } from './export-exercise.jsx'
import { Exercise } from '../exercise.js'
import { useState, useEffect } from 'react'

export function Build(props) {
  const [exercise, setExercise] = useState(new Exercise())

  const loadExercise = (data, resources) => {
    const newExercise = new Exercise()
    newExercise.load(data, resources)
    setExercise(newExercise)
    props.onLoad(newExercise)
  }

  const refreshExercise = (part) => {
    props.onUpdate(part)
  }

  useEffect(() => {
    props.onLoad(exercise)
  }, [])

  return (
    <>
      <LoadExercise onLoad={loadExercise}/>
      <ExerciseParts exercise={exercise} onUpdate={refreshExercise}/>
      <ExportExercise exercise={exercise}/>
    </>
  )
}
