import exampleData from '../../../examples/*.js'
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css';
import {useRef} from 'react';
import * as loadManager from '../load-manager.js'
import * as scormManager from '../scorm-manager.js'

const examples = new Map(Object.entries(exampleData))

export function LoadExercise(props) {

  const mainMenu = useRef(null)
  const exampleMenu = useRef(null)
  const fileInput = useRef(null)

  const closeMenus = () => {
    exampleMenu.current._tippy.hide()
    mainMenu.current._tippy.hide()
  }

  const loadExample = async (name) => {
    if (examples.has(name)) {
      const data = examples.get(name)
      const resources = data.resources
      const localResources = await loadManager.loadResources(resources)
      props.onLoad(examples.get(name), localResources)
    }
  }

  const loadFile = () => {
    closeMenus()
    fileInput.current.click()
  }

  const readFile = async () => {
    if (fileInput.current.files) {
      const file = fileInput.current.files[0]
      const scormContent = await loadManager.readFileAsync(file)
      const exerciseData = await scormManager.loadSCORMFile(scormContent)
      const localResources = await loadManager.loadResources(exerciseData.resources,exerciseData.zipFile)
      props.onLoad(exerciseData, localResources)
    }
  }

  const exampleItems = [...examples.keys()].map(example => <li key={example} onClick={() => {closeMenus(); loadExample(example);}}>{example}</li>)

  return (
    <div id="input">
      <Tippy ref={mainMenu} content= {
        <ul  className="build-menu-items">
          <Tippy ref={exampleMenu} content= {
            <ul id="build-examples-list" className="build-menu-items">
              {exampleItems}
            </ul>
          } placement="right-start" trigger="click" arrow={false} interactive={true} theme="light">
            <li id="build-select-example">Sélectionner un exemple...</li>
          </Tippy>
          <li id="build-import" onClick={loadFile}>Importer SCORM...</li>
        </ul>
      } placement="bottom-start" trigger="click" arrow={false} interactive={true} theme="light">
        <button id="open"><i className="icon-folder"></i> Charger un exercice</button>
      </Tippy>
      <input ref={fileInput} type="file" onChange={readFile} className="hidden"/>
    </div>
  )
}
