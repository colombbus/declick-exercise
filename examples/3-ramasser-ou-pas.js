const version = '1'

const title = 'Ramasser ou pas'

const instructions = `Le robot ne ramasse plus automatiquement les étoiles : il faut utiliser la commande \`ramasser()\`.

Mais il ne faut l'utiliser que sur une étoile !

Les commandes entre les accolades ne sont exécutées que si le robot est sur une étoile :

\`\`\`
if (etoile)
{
  ramasser()
}
\`\`\`

Complète le programme pour que le robot ramasse toutes les étoiles`

const resources = {
  briques: ['image', 'tiles.png'],
  labyrinthe: ['map', 'maze3.json'],
}

const init = `p = new Plateforme('labyrinthe', 'briques')
bob = new Robot()
bob.définirLongueurPas(40)
bob.définirPosition(1,2)
etoile = false
nombre = 5
function avancer() {
  etoile = false
  bob.avancer()
}
function monter() {
  etoile = false
  bob.monter()
}
function descendre() {
  etoile = false
  bob.descendre()
}
function reculer() {
  etoile = false
  bob.reculer()
}
function test(moi, brique) {
  etoile = true
}
function stop() {
  error("Le robot a tapé les murs !")
  declick.arrêter()
}
function pasEtoile() {
  error("Le robot n'est pas sur une étoile !")
  declick.arrêter()
}
function ramasser() {
  if (etoile) {
    brique = p.récupérerBrique(bob.récupérerX(), bob.récupérerY())
    if (brique.est('star')) {
      p.retirerBrique(brique)
      nombre --
    } else {
      pasEtoile()
    }
  } else {
    pasEtoile()
  }
}
bob.définirRectangleCollision(10,10)
bob.siRencontre(p, test, "star")
bob.siCollisionAvec(p, stop)`

const code = `répéter(8) {
  // à compléter
}`

const check = `//require nombre
if (!code.has({type:"RepeatStatement"})) {
  return 'il faut utiliser répéter'
}
if (code.length > 1) {
  return 'toutes les commandes doivent être dans répéter !'
}
let bodyCode = code.getBody(0)
if (!bodyCode.has({type:"IfStatement"})) {
  return 'il faut utiliser if'
}
if (nombre > 0) {
  return 'il reste des étoiles !'
}
return true
`

export { title, instructions, init, code, version, check, resources }
