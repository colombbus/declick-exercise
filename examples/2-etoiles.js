const version = '1'

const title = 'Des étoiles !'

const instructions = `# Ramasser toutes les étoiles #

En gardant le même nombre de commandes, peux-tu faire en sorte que le robot ramasse toutes les étoiles ?

Attention à ne pas se cogner aux murs !`

const resources = {
  briques: ['image', 'tiles.png'],
  labyrinthe: ['map', 'maze.json'],
}

const init = `p = new Plateforme('labyrinthe', 'briques')
bob = new Robot()
bob.définirLongueurPas(40)
bob.définirPosition(1,1)
etoiles = 7
function avancer() {
  bob.avancer()
}
function monter() {
  bob.monter()
}
function descendre() {
  bob.descendre()
}
function reculer() {
  bob.reculer()
}
function miam(moi, brique) {
  p.retirerBrique(brique)
  etoiles--
}
function stop() {
  error("Le robot a tapé les murs !")
  declick.arrêter()
}
bob.définirRectangleCollision(30,30)
bob.siRencontre(p, miam, "star")
bob.siCollisionAvec(p, stop)`

const code = `répéter(3){
  avancer()
  reculer()
}`

const check = `//require etoiles
if (!code.has({type:"RepeatStatement"})) {
  return 'il faut utiliser répéter'
}
if (code.length > 1) {
  return 'il y a trop de commandes !'
}
if (etoiles > 0) {
  return 'il reste des étoiles !'
}
return true
`

export { title, instructions, init, code, version, check, resources }
