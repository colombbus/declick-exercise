const version = '1'

const title = 'Dessine moi un escalier'

const instructions = `# Terminer le dessin #
Complète le programme pour que le stylo dessine l'escalier.`

const init = `stairs = new Image('stairs')
stairs.définirPosition(100,68)
stylo = new Tortue('pen')
stylo.définirPosition(2, 0)
stylo.tracer()
stylo.définirPositionTraceur(2, 72)`

const code = `stylo.avancer()
stylo.descendre()
stylo.avancer()
`

const resources = {
  pen: ['image', 'pen.png'],
  stairs: ['image', 'stairs.png'],
}

const check = `//require stylo
if (stylo.getY() !== 2 || stylo.getY() !== 2) {
  return "le stylo ne s'est pas arrêté en bas de l'escalier"
}
if (code.length > 5) {
  return 'tu as écrit trop de commandes !'
}
if (!code.isLike('stylo.avancer()\\nstylo.descendre()\\nstylo.avancer()\\nstylo.descendre()\\nstylo.avancer()')) {
  return "le dessin n'est pas le bon"
}
return true
`

export { title, instructions, init, code, version, resources, check }
