const version = "1";

const title = "Avancer";

const instructions = `# Faire avancer le robot #
Voici un robot : lorsque tu cliques sur le bouton Exécuter, il descend.

Sauras-tu compléter le programme pour qu'il avance sur la croix  ?`;

const init = `croix = new Image('cross')
croix.définirPosition(52, 102)
bob = new Robot()
function descendre() {
  bob.descendre()
}
function avancer() {
  bob.avancer()
}`;

const code = `descendre()
descendre()`;

const resources = {
  cross: ["image", "cross.png"],
};

const check = `//require bob
if (code.length > 3) {
  return 'tu as fait trop compliqué !'
}
if (bob.getX()===0) {
  return 'le robot devrait avancer'
}
if (bob.getX()>1) {
  return 'le robot a avancé trop loin'
}
if (bob.getY()<2) {
  return 'le robot n\\'est pas descendu assez bas'
}
if (bob.getY()>2) {
  return 'le robot est descendu trop bas'
}
return true`;

export { title, instructions, init, code, version, resources, check };
