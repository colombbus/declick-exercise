const version = '1'

const title = 'Avancer ou pas'

const instructions = `# Atteindre l'étoile #

Dans le code suivant, \`commande1\` est exécutée si le robot peut avancer, \`commande2\` est exécutée dans le cas contraire.
\`\`\`

if (peutAvancer)
{
  commande1
}
else
{
  commande2
}
\`\`\`
Trouve la **commande 1** et la **commande 2** qui permettront au robot d'atteindre l'étoile.`

const resources = {
  briques: ['image', 'tiles.png'],
  labyrinthe: ['map', 'maze2.json'],
}

const init = `p = new Plateforme('labyrinthe', 'briques')
bob = new Robot()
bob.définirLongueurPas(40)
bob.définirPosition(1,1)
peutAvancer = true

function avancer() {
  bob.avancer()
  calcule()
}

function calcule() {
  brique = p.récupérerBrique(bob.récupérerX()+1, bob.récupérerY())
  peutAvancer = !brique.est('wall')
}

function monter() {
  bob.monter()
  calcule()
}

function descendre() {
  bob.descendre()
  calcule()
}

function reculer() {
  bob.reculer()
  calcule()
}

function miam(moi, brique) {
  p.retirerBrique(brique)
  etoile = true
}

function stop() {
  error("Le robot a tapé les murs !")
  declick.arrêter()
}

bob.définirRectangleCollision(10,10)
bob.siRencontre(p, miam, "star")
bob.siCollisionAvec(p, stop)`

const code = `répéter(15) {
  if (peutAvancer)
  {
    // commande 1
  }
  else
  {
    // commande 2
  }
}`

const check = `//require etoile
if (code.length > 1) {
  return 'il y a trop de commandes !'
}
if (!code.has({type:"RepeatStatement"})) {
  return 'il faut utiliser répéter'
}
let bodyCode = code.getBody(0)
if (!bodyCode.has({type:"IfStatement"})) {
  return 'il faut utiliser if'
}
if (!etoile) {
  return 'il faut atteindre l\\'étoile'
}
return true
`

export { title, instructions, init, code, version, check, resources }
